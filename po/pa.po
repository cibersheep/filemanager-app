# Punjabi translation for ubuntu-filemanager-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the ubuntu-filemanager-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-filemanager-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-03 13:35+0000\n"
"PO-Revision-Date: 2015-02-10 05:33+0000\n"
"Last-Translator: Gursharnjit_Singh <ubuntuser13@gmail.com>\n"
"Language-Team: Punjabi <pa@li.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2017-04-08 06:04+0000\n"
"X-Generator: Launchpad (build 18343)\n"

#: ../src/app/qml/actions/AddBookmark.qml:6
msgid "Add bookmark"
msgstr ""

#: ../src/app/qml/actions/ArchiveExtract.qml:6
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:18
#: ../src/app/qml/dialogs/OpenWithDialog.qml:33
msgid "Extract archive"
msgstr "ਅਕਾਈਵ ਖਿਲਾਰੋ"

#: ../src/app/qml/actions/Delete.qml:5
#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:14
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:15
#: ../src/app/qml/panels/SelectionBottomBar.qml:26
msgid "Delete"
msgstr "ਹਟਾਓ"

#: ../src/app/qml/actions/FileClearSelection.qml:8
msgid "Clear clipboard"
msgstr "ਕਲਿੱਪਬੋਰਡ ਸ਼ਾਫ਼ ਕਰੋ"

#: ../src/app/qml/actions/FileCopy.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:45
msgid "Copy"
msgstr "ਕਾਪੀ ਕਰੋ"

#: ../src/app/qml/actions/FileCut.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:60
msgid "Cut"
msgstr "ਕੱਟ"

#: ../src/app/qml/actions/FilePaste.qml:30
#, fuzzy, qt-format
msgid "Paste %1 file"
msgid_plural "Paste %1 files"
msgstr[0] "ਚੇਪੋ %1 ਫ਼ਾਈਲ"
msgstr[1] "ਚੇਪੋ %1 ਫ਼ਾਈਲਾਂ"

#: ../src/app/qml/actions/GoBack.qml:5
msgid "Go back"
msgstr ""

#: ../src/app/qml/actions/GoNext.qml:5
msgid "Go next"
msgstr ""

#: ../src/app/qml/actions/GoTo.qml:6
msgid "Go To"
msgstr "ਤੇ ਜਾਓ"

#: ../src/app/qml/actions/NewItem.qml:6
msgid "New Item"
msgstr ""

#: ../src/app/qml/actions/PlacesBookmarks.qml:6
#: ../src/app/qml/ui/PlacesPage.qml:16
msgid "Places"
msgstr "ਥਾਵਾਂ"

#: ../src/app/qml/actions/Properties.qml:6
#: ../src/app/qml/dialogs/OpenWithDialog.qml:54
msgid "Properties"
msgstr "ਵਿਸ਼ੇਸ਼ਤਾ"

#: ../src/app/qml/actions/Rename.qml:5
#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:20
msgid "Rename"
msgstr "ਨਾਂ ਬਦਲੋ"

#: ../src/app/qml/actions/Select.qml:5
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:60
msgid "Select"
msgstr "ਚੁਣੋ"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
#, fuzzy
msgid "Select None"
msgstr "ਚੁਣੋ"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
#, fuzzy
msgid "Select All"
msgstr "ਚੁਣੋ"

#: ../src/app/qml/actions/Settings.qml:6
msgid "Settings"
msgstr "ਸੈਟਿੰਗ"

#: ../src/app/qml/actions/Share.qml:5
msgid "Share"
msgstr ""

#: ../src/app/qml/actions/TabsAdd.qml:5
msgid "Add tab"
msgstr ""

#: ../src/app/qml/actions/TabsCloseThis.qml:5
msgid "Close this tab"
msgstr "ਇਹ ਟੈਬ ਬੰਦ ਕਰੋ"

#: ../src/app/qml/actions/TabsOpenInNewTab.qml:5
msgid "Open in a new tab"
msgstr "ਨਵੀ ਟੈਬ ਵਿੱਚ ਖੋਲ੍ਹੋ"

#: ../src/app/qml/actions/UnlockFullAccess.qml:6
msgid "Unlock"
msgstr ""

#: ../src/app/qml/authentication/FingerprintDialog.qml:26
#: ../src/app/qml/authentication/PasswordDialog.qml:43
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:28
msgid "Authentication required"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਦੀ ਲੋੜ"

#: ../src/app/qml/authentication/FingerprintDialog.qml:27
msgid "Use your fingerprint to access restricted content"
msgstr ""

#. TRANSLATORS: "Touch" here is a verb
#: ../src/app/qml/authentication/FingerprintDialog.qml:55
msgid "Touch sensor"
msgstr ""

#: ../src/app/qml/authentication/FingerprintDialog.qml:60
#, fuzzy
msgid "Use password"
msgstr "ਪਾਸਵਰਡ"

#: ../src/app/qml/authentication/FingerprintDialog.qml:69
#: ../src/app/qml/authentication/PasswordDialog.qml:80
#: ../src/app/qml/dialogs/CreateItemDialog.qml:60
#: ../src/app/qml/dialogs/ExtractingDialog.qml:35
#: ../src/app/qml/dialogs/FileActionDialog.qml:45
#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:44
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:96
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:38
#: ../src/app/qml/dialogs/OpenWithDialog.qml:64
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:41
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:60
#: ../src/app/qml/ui/FileDetailsPopover.qml:184
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:51
#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:26
msgid "Cancel"
msgstr "ਰੱਦ"

#: ../src/app/qml/authentication/FingerprintDialog.qml:126
#, fuzzy
msgid "Authentication failed!"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਫ਼ੇਲ੍ਹ ਹੋਈ"

#: ../src/app/qml/authentication/FingerprintDialog.qml:137
msgid "Please retry"
msgstr ""

#: ../src/app/qml/authentication/PasswordDialog.qml:35
msgid "Authentication failed"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਫ਼ੇਲ੍ਹ ਹੋਈ"

#: ../src/app/qml/authentication/PasswordDialog.qml:45
#, fuzzy
msgid "Your passphrase is required to access restricted content"
msgstr "ਸਾਰੀਆਂ ਫ਼ਾਈਲਾਂ ਤੱਕ ਪਹੁੰਚ ਲਈ ਪਾਸਵਰਡ ਦੀ ਲੋੜ"

#: ../src/app/qml/authentication/PasswordDialog.qml:46
#, fuzzy
msgid "Your passcode is required to access restricted content"
msgstr "ਸਾਰੀਆਂ ਫ਼ਾਈਲਾਂ ਤੱਕ ਪਹੁੰਚ ਲਈ ਪਾਸਵਰਡ ਦੀ ਲੋੜ"

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passphrase (default is 0000 if unset)"
msgstr ""

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passcode (default is 0000 if unset)"
msgstr ""

#: ../src/app/qml/authentication/PasswordDialog.qml:69
#, fuzzy
msgid "Authentication failed. Please retry"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਫ਼ੇਲ੍ਹ ਹੋਈ"

#: ../src/app/qml/authentication/PasswordDialog.qml:74
#, fuzzy
msgid "Authenticate"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਫ਼ੇਲ੍ਹ ਹੋਈ"

#: ../src/app/qml/backend/FolderListModel.qml:123
#, fuzzy, qt-format
msgid "%1 file"
msgstr "%1 ਫਾਈਲ"

#: ../src/app/qml/backend/FolderListModel.qml:124
#, fuzzy, qt-format
msgid "%1 files"
msgstr "%1 ਫਾਈਲ"

#: ../src/app/qml/backend/FolderListModel.qml:135
#, fuzzy
msgid "Folder"
msgstr "ਨਵਾਂ ਫੋਲਡਰ"

#: ../src/app/qml/components/TextualButtonStyle.qml:48
#: ../src/app/qml/components/TextualButtonStyle.qml:56
#: ../src/app/qml/components/TextualButtonStyle.qml:63
msgid "Pick"
msgstr ""

#: ../src/app/qml/content-hub/FileOpener.qml:27
msgid "Open with"
msgstr "ਖੋਲ੍ਹੋ ਨਾਲ"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:15
msgid "Extract Archive"
msgstr "ਅਕਾਈਵ ਖਿਲਾਰੋ"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:16
#, qt-format
msgid "Are you sure you want to extract '%1' here?"
msgstr "ਕੀ ਤੁਸੀਂ '%1' ਇੱਥੇ ਖਿਲਾਰਣ ਕਰਨ ਦੀ ਪੁਸ਼ਟੀ ਕਰਦੇ ਹੋ?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:16
#, qt-format
msgid "Are you sure you want to permanently delete '%1'?"
msgstr "ਕੀ ਤੁਹਾਨੂੰ ਯਕੀਨ ਹੈ ਤੁਸੀ ਪੱਕੇ ਤੌਰ ਤੇ ਹਟਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ '%1'?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
#, fuzzy
msgid "these files"
msgstr "ਫ਼ਾਈਲਾਂ ਪੇਸਟ ਕਰੋ"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:18
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:21
msgid "Deleting files"
msgstr "ਫ਼ਾਈਲਾਂ ਹਟਾ ਰਿਹਾ"

#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:21
msgid "Enter a new name"
msgstr "ਇੱਕ ਨਵਾਂ ਨਾਂ ਭਰੋ"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:13
#, fuzzy
msgid "Create Item"
msgstr "ਫ਼ਾਈਲ ਬਣਾਓ"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:14
#, fuzzy
msgid "Enter name for new item"
msgstr "ਨਵੀ ਫ਼ਾਈਲ ਲਈ ਨਾਂ ਭਰੋ"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:18
#, fuzzy
msgid "Item name"
msgstr "ਨਾਂ ਬਦਲੋ"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:24
msgid "Create file"
msgstr "ਫ਼ਾਈਲ ਬਣਾਓ"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:43
#, fuzzy
msgid "Create Folder"
msgstr "ਫੋਲਡਰ ਬਣਾਓ"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:23
#, qt-format
msgid "Extracting archive '%1'"
msgstr "'%1' ਅਕਾਈਵ ਖਿਲਾਰ ਰਿਹਾ"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:44
#: ../src/app/qml/dialogs/NotifyDialog.qml:27
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:31
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:49
msgid "OK"
msgstr "ਠੀਕ"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:59
msgid "Extracting failed"
msgstr "ਖਿਲਾਰਣਾ ਫ਼ੇਲ੍ਹ ਹੋਇਆ"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:60
#, qt-format
msgid "Extracting the archive '%1' failed."
msgstr "ਅਕਾਈਵ '%1' ਖਿਲਾਰਣਾ ਫ਼ੇਲ੍ਹ ਹੋਇਆ।"

#: ../src/app/qml/dialogs/FileActionDialog.qml:30
msgid "Choose action"
msgstr "ਕਾਰਵਾਈ ਚੁਣੋ"

#: ../src/app/qml/dialogs/FileActionDialog.qml:31
#, qt-format
msgid "For file: %1"
msgstr "ਫ਼ਾਈਲ ਲਈ: %1"

#: ../src/app/qml/dialogs/FileActionDialog.qml:35
msgid "Open"
msgstr "ਖੋਲ੍ਹੋ"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:27
msgid "Operation in progress"
msgstr "ਕਾਰਵਾਈ ਜਾਰੀ ਹੈ"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:29
msgid "File operation"
msgstr "ਫ਼ਾਈਲ ਕਾਰਵਾਈ"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:45
msgid "User"
msgstr ""

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:58
msgid "Password"
msgstr ""

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:72
msgid "Save password"
msgstr ""

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:108
msgid "Ok"
msgstr "ਠੀਕ"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:8
msgid "Archive file"
msgstr "ਅਕਾਈਵ ਫ਼ਾਈਲ"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:9
msgid "Do you want to extract the archive here?"
msgstr "ਕੀ ਤੁਸੀਂ ਅਕਾਈਵ ਇੱਥੇ ਖਿਲੇਰਣਾ ਚਾਹੁੰਦੇ ਹੋ?"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:28
#: ../src/app/qml/dialogs/OpenWithDialog.qml:44
msgid "Open with another app"
msgstr "ਹੋਰ ਐਪ ਨਾਲ ਖੋਲ੍ਹੋ"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:8
#, fuzzy
msgid "Open file"
msgstr "ਖੋਲ੍ਹੋ ਨਾਲ"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:9
#, fuzzy
msgid "What do you want to do with the clicked file?"
msgstr "ਕੀ ਤੁਸੀਂ ਅਕਾਈਵ ਇੱਥੇ ਖਿਲੇਰਣਾ ਚਾਹੁੰਦੇ ਹੋ?"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:22
msgid "Preview"
msgstr ""

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "File %1"
msgstr ""

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "%1 Files"
msgstr ""

#: ../src/app/qml/filemanager.qml:171
#, qt-format
msgid "Saved to: %1"
msgstr ""

#: ../src/app/qml/panels/DefaultBottomBar.qml:24
msgid "Paste files"
msgstr "ਫ਼ਾਈਲਾਂ ਪੇਸਟ ਕਰੋ"

#: ../src/app/qml/ui/FileDetailsPopover.qml:34
msgid "Readable"
msgstr "ਪੜ੍ਹਨਯੋਗ"

#: ../src/app/qml/ui/FileDetailsPopover.qml:37
msgid "Writable"
msgstr "ਲਿਖਣਯੋਗ"

#: ../src/app/qml/ui/FileDetailsPopover.qml:40
msgid "Executable"
msgstr "ਚੱਲਣਯੋਗ"

#: ../src/app/qml/ui/FileDetailsPopover.qml:100
msgid "Where:"
msgstr ""

#: ../src/app/qml/ui/FileDetailsPopover.qml:127
#, fuzzy
msgid "Created:"
msgstr "ਫੋਲਡਰ ਬਣਾਓ"

#: ../src/app/qml/ui/FileDetailsPopover.qml:141
msgid "Modified:"
msgstr "ਸੋਧਿਆ:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:155
msgid "Accessed:"
msgstr "ਪਹੁੰਚ ਕੀਤੀ:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:176
msgid "Permissions:"
msgstr "ਅਾਗਿਆ:"

#: ../src/app/qml/ui/FolderListPage.qml:241
#: ../src/app/qml/ui/FolderListPage.qml:296
msgid "Restricted access"
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:245
msgid ""
"Authentication is required in order to see all the content of this folder."
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:282
msgid "No files"
msgstr "ਕੋਈ ਫਾਈਲ ਨਹੀਂ"

#: ../src/app/qml/ui/FolderListPage.qml:283
msgid "This folder is empty."
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:297
msgid "Authentication is required in order to see the content of this folder."
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:313
msgid "File operation error"
msgstr "ਫ਼ਾਈਲ ਕਾਰਵਾਈ ਖਾਮੀ"

#: ../src/app/qml/ui/FolderListPageDefaultHeader.qml:21
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
#, fuzzy, qt-format
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] "ਚੀਜ਼ਾਂ"
msgstr[1] "ਚੀਜ਼ਾਂ"

#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
msgid "Save here"
msgstr ""

#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:18
#, qt-format
msgid "%1 item selected"
msgid_plural "%1 items selected"
msgstr[0] ""
msgstr[1] ""

#: ../src/app/qml/ui/ViewPopover.qml:24
msgid "Show Hidden Files"
msgstr "ਲੁਕਵੀਆਂ ਫਾਈਲਾਂ ਵਿਖਾਓ"

#: ../src/app/qml/ui/ViewPopover.qml:37
msgid "View As"
msgstr "ਦੇ ਤੌਰ ਤੇ ਵੇਖੋ"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "List"
msgstr "ਸੂਚੀ"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "Icons"
msgstr "ਆਈਕਾਨ"

#: ../src/app/qml/ui/ViewPopover.qml:44
msgid "Grid size"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "S"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "M"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "L"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "XL"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:52
#, fuzzy
msgid "List size"
msgstr "ਸੂਚੀ"

#: ../src/app/qml/ui/ViewPopover.qml:60
msgid "Sort By"
msgstr "ਦੁਆਰਾ ਕ੍ਰਮਬੱਧ"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Name"
msgstr "ਨਾਂ"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Date"
msgstr "ਤਾਰੀਖ"

#: ../src/app/qml/ui/ViewPopover.qml:62
#, fuzzy
msgid "Size"
msgstr "ਅਕਾਰ:"

#: ../src/app/qml/ui/ViewPopover.qml:67
msgid "Sort Order"
msgstr "ਤਰਤੀਬ ਕ੍ਰਮਬੱਧ"

#: ../src/app/qml/ui/ViewPopover.qml:74
msgid "Theme"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Light"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Dark"
msgstr ""

#: ../src/app/qml/views/FolderDelegateActions.qml:40
msgid "Folder not accessible"
msgstr "ਫ਼ੋਲਡਰ ਪਹੁੰਚ ਕਰਨ ਯੋਗ ਨਹੀ"

#. TRANSLATORS: this refers to a folder name
#: ../src/app/qml/views/FolderDelegateActions.qml:42
#, qt-format
msgid "Can not access %1"
msgstr "ਪਹੁੰਚ ਨਹੀ ਸਕਦਾ %1"

#: ../src/app/qml/views/FolderDelegateActions.qml:136
msgid "Could not rename"
msgstr "ਨਾਂ ਨਹੀ ਬਦਲ ਸਕਦਾ"

#: ../src/app/qml/views/FolderDelegateActions.qml:137
#, fuzzy
msgid ""
"Insufficient permissions, name contains special chars (e.g. '/'), or already "
"exists"
msgstr "ਨਾਕਾਫ਼ੀ ਮਨਜ਼ੂਰੀ ਜਾਂ ਨਾਂ ਪਹਿਲਾਂ ਤੋਂ ਮੌਜੂਦ?"

#: ../src/app/qml/views/FolderListView.qml:72
msgid "Directories"
msgstr ""

#: ../src/app/qml/views/FolderListView.qml:72
#, fuzzy
msgid "Files"
msgstr "ਕੋਈ ਫਾਈਲ ਨਹੀਂ"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:360
msgid "Unknown"
msgstr ""

#: ../src/plugin/folderlistmodel/dirmodel.cpp:487
msgid "path or url may not exist or cannot be read"
msgstr "ਰਸਤਾ ਜਾਂ url ਮੌਜੂਦ ਨਹੀਂ ਜਾਂ ਪੜ੍ਹੇ ਨਹੀਂ ਜਾ ਸਕਦੇ"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:690
msgid "Rename error"
msgstr "ਮੁੜ-ਨਾਂ ਖਾਮੀ"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:713
msgid "Error creating new folder"
msgstr "ਨਵਾਂ ਫ਼ੋਲਡਰ ਬਣਾਉਂਦੇ ਸਮੇਂ ਖਾਮੀ"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:740
msgid "Touch file error"
msgstr ""

#: ../src/plugin/folderlistmodel/dirmodel.cpp:1353
msgid "items"
msgstr "ਚੀਜ਼ਾਂ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:320
msgid "File or Directory does not exist"
msgstr "ਫ਼ਾਈਲ ਜਾਂ ਡਾਇਰੈਕਟਰੀ ਮੌਜੂਦ ਨਹੀਂ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:321
msgid " does not exist"
msgstr " ਮੌਜੂਦ ਨਹੀਂ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:326
msgid "Cannot access File or Directory"
msgstr ""

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:327
msgid " it needs Authentication"
msgstr ""

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:361
msgid "Cannot copy/move items"
msgstr ""

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:362
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1487
msgid "no write permission on folder "
msgstr "ਫ਼ੋਲਡਰ ਤੇ ਲਿੱਖਣ ਦਾ ਅਧਿਕਾਰ ਨਹੀਂ "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:642
msgid "Could not remove the item "
msgstr "ਚੀਜ਼ ਹਟਾ ਨਹੀਂ ਸਕਦਾ "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:680
msgid "Could not find a suitable name to backup"
msgstr "ਬੈੱਕਅਪ ਲਈ ਠੀਕ ਨਾਂ ਨਹੀਂ ਲੱਭ ਸਕਿਆ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:733
msgid "Could not create the directory"
msgstr "ਡਾਇਰੈਕਟਰੀ ਬਣਾ ਨਹੀਂ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:738
msgid "Could not create link to"
msgstr "ਲਈ ਲਿੰਕ ਬਣਾ ਨਹੀਂ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:746
msgid "Could not set permissions to dir"
msgstr "dir ਲਈ ਅਧਿਕਾਰ ਸੈੱਟ ਨਹੀਂ ਕਰ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:756
msgid "Could not open file"
msgstr "ਫ਼ਾਈਲ ਖੋਲ੍ਹ ਨਹੀਂ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:772
msgid "There is no space to copy"
msgstr ""

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:780
msgid "Could not create file"
msgstr "ਫ਼ਾਈਲ ਨਹੀਂ ਬਣਾ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:837
msgid "Could not remove the directory/file "
msgstr "ਡਾਇਰੈਕਟਰੀ/ਫ਼ਾਈਲ ਨਹੀਂ ਹਟਾ ਸਕਦਾ "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:850
msgid "Could not move the directory/file "
msgstr "ਡਾਇਰੈਕਟਰੀ/ਫ਼ਾਈਲ ਮੂਵ ਨਹੀਂ ਕਰ ਸਕਦਾ "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1089
msgid "Write error in "
msgstr "ਵਿੱਚ ਲਿੱਖਣ ਖਾਮੀ "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1101
msgid "Read error in "
msgstr "ਵਿੱਚ ਪੜ੍ਹਣ ਖਾਮੀ "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1318
msgid " Copy"
msgstr " ਕਾਪੀ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1371
msgid "Set permissions error in "
msgstr "ਵਿੱਚ ਅਧਿਕਾਰ ਸੈੱਟ ਕਰਨ ਵਿੱਚ ਖਾਮੀ "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1437
msgid "Could not create trash info file"
msgstr "ਟਰੈਸ ਇੰਫ਼ੋ ਫ਼ਾਈਲ ਨਹੀਂ ਰਚ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1449
msgid "Could not remove the trash info file"
msgstr "ਟਰੈਸ ਇੰਫ਼ੋ ਫ਼ਾਈਲ ਨਹੀਂ ਹਟਾ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1479
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1486
msgid "Cannot move items"
msgstr "ਚੀਜ਼ਾਂ ਮੂਵ ਨਹੀਂ ਕਰ ਸਕਦਾ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1481
msgid "origin and destination folders are the same"
msgstr "ਮੂਲ ਅਤੇ ਮੰਜ਼ਿਲ ਫ਼ੋਲਡਰ ਇੱਕੋ ਹਨ"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1524
msgid "There is no space to download"
msgstr ""

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:72
msgid "net tool not found, check samba installation"
msgstr ""

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:80
msgid "cannot write in "
msgstr ""

#: com.ubuntu.filemanager.desktop.in.in.h:1
msgid "File Manager"
msgstr "ਫ਼ਾਈਲ ਮੈਨੇਜਰ"

#: com.ubuntu.filemanager.desktop.in.in.h:2
msgid "folder;manager;explore;disk;filesystem;"
msgstr ""

#~ msgid "%1 (%2 file)"
#~ msgid_plural "%1 (%2 files)"
#~ msgstr[0] "%1 (%2 ਫ਼ਾਈਲ)"
#~ msgstr[1] "%1 (%2 ਫ਼ਾਈਲਾਂ)"

#~ msgid "Device"
#~ msgstr "ਜੰਤਰ"

#~ msgid "Change app settings"
#~ msgstr "ਐਪ ਸੈਟਿੰਗ ਬਦਲੋ"

#~ msgid "Path:"
#~ msgstr "ਰਾਹ:"

#~ msgid "Contents:"
#~ msgstr "ਸਮੱਗਰੀ:"

#~ msgid "Unlock full access"
#~ msgstr "ਮੁਕੰਮਲ ਪਹੁੰਚ ਖੋਲ੍ਹੋ"

#~ msgid "Enter name for new folder"
#~ msgstr "ਨਵੇ ਫ਼ੋਲਡਰ ਲਈ ਨਾਂ ਭਰੋ"

#~ msgid "~/Desktop"
#~ msgstr "~/ਡੈਸਕਟੋਪ"

#~ msgid "~/Public"
#~ msgstr "~/ਆਮ"

#~ msgid "~/Programs"
#~ msgstr "~/ਪ੍ਰੋਗਰਾਮ"

#~ msgid "~/Templates"
#~ msgstr "~/ਤਿਅਾਰ ਖਾਕੇ"

#~ msgid "Home"
#~ msgstr "ਘਰ"

#~ msgid "Go To Location"
#~ msgstr "ਥਾਂ ਤੇ ਜਾਓ"

#~ msgid "Enter a location to go to:"
#~ msgstr "ਜਾਣ ਲਈ ਇੱਕ ਥਾਂ ਲਿੱਖੋ:"

#~ msgid "Location..."
#~ msgstr "ਸਥਿਤੀ..."

#~ msgid "Go"
#~ msgstr "ਜਾਓ"

#~ msgid "Show Advanced Features"
#~ msgstr "ਉਂਨਤ ਵਿਸ਼ੇਸਤਾਵਾਂ ਵਿਖਾਓ"

#~ msgid "Ascending"
#~ msgstr "ਵੱਧਦਾ ਕ੍ਰਮ"

#~ msgid "Descending"
#~ msgstr "ਘੱਟਦਾ ਕ੍ਰਮ"

#~ msgid "Filter"
#~ msgstr "ਫਿਲਟਰ"
